use std::collections::HashSet;

pub fn find_marker(input: &str, len: usize) -> usize {
    for (index, window) in input.chars().collect::<Vec<char>>().windows(len).enumerate() {
        if HashSet::<_>::from_iter(window.iter().cloned()).len() == len {
            println!("Found marker {}", window.iter().collect::<String>());
            return index + len
        }
    }
    panic!("No marker found");
}

#[aoc(day6, part1)]
pub fn part1(input: &str) -> usize {
    find_marker(input, 4)
}

#[aoc(day6, part2)]
pub fn part2(input: &str) -> usize {
    find_marker(input, 14)
}