#[derive(Debug, Copy, Clone)]
pub enum Hand {
    Rock,
    Paper,
    Scissors,
}

#[derive(Debug, Copy, Clone)]
pub enum Outcome {
    Win,
    Lose,
    Draw,
}

#[derive(Debug, Copy, Clone)]
pub struct Match {
    pub player1: Hand,
    pub player2: Option<Hand>,
    pub outcome: Option<Outcome>,
}

impl Match {
    pub fn new(player1: Hand, player2: Option<Hand>, outcome: Option<Outcome>) -> Self {
        Self { player1, player2, outcome }
    }

    pub fn play(&self) -> u32 {
        match (self.player1, self.player2) {
            (Hand::Rock, Some(Hand::Paper)) => 8,
            (Hand::Rock, Some(Hand::Scissors)) => 3,
            (Hand::Rock, Some(Hand::Rock)) => 4,
            (Hand::Paper, Some(Hand::Paper)) => 5,
            (Hand::Paper, Some(Hand::Scissors)) => 9,
            (Hand::Paper, Some(Hand::Rock)) => 1,
            (Hand::Scissors, Some(Hand::Paper)) => 2,
            (Hand::Scissors, Some(Hand::Scissors)) => 6,
            (Hand::Scissors, Some(Hand::Rock)) => 7,
            _ => unreachable!()
        }
    }

    pub fn fix(&self) -> u32 {
        match (self.player1, self.outcome.unwrap()) {
            (Hand::Rock, Outcome::Win) => 8, // Paper beats Rock => 2 + 6 = 8
            (Hand::Rock, Outcome::Lose) => 3, // Scissors loses to Rock => 3 + 0 = 3
            (Hand::Rock, Outcome::Draw) => 4, // Rock draws with Rock => 1 + 3 = 4
            (Hand::Paper, Outcome::Win) => 9, // Scissors beats Paper => 3 + 6 = 9
            (Hand::Paper, Outcome::Lose) => 1, // Rock loses to Paper => 1 + 0 = 1
            (Hand::Paper, Outcome::Draw) => 5, // Paper draws with Paper => 2 + 3 = 5
            (Hand::Scissors, Outcome::Win) => 7, // Rock beats Scissors => 1 + 6 = 7
            (Hand::Scissors, Outcome::Lose) => 2, // Paper loses to Scissors => 2 + 0 = 2
            (Hand::Scissors, Outcome::Draw) => 6, // Scissors draws with Scissors => 3 + 3 = 6
        }
    }
}


#[aoc_generator(day2, part1)]
pub fn part1_generator(input: &str) -> Vec<Match> {
    input.split('\n').map(|line| {
        let mut hands = line.split(' ');
        let player1 = hands.next().unwrap();
        let player2 = hands.next().unwrap();

        let player1 = match player1 {
            "A" => Hand::Rock,
            "B" => Hand::Paper,
            "C" => Hand::Scissors,
            _ => panic!("Invalid hand"),
        };

        let player2 = match player2 {
            "X" => Hand::Rock,
            "Y" => Hand::Paper,
            "Z" => Hand::Scissors,
            _ => panic!("Invalid hand"),
        };

        Match::new(player1, Some(player2), None)
    }).collect()
}

#[aoc(day2, part1)]
pub fn part1(input: &[Match]) -> u32 {
    input.iter().map(|m| m.play()).sum()
}

#[aoc_generator(day2, part2)]
pub fn part2_generator(input: &str) -> Vec<Match> {
    input.split('\n').map(|line| {
        let mut hands = line.split(' ');
        let player1 = hands.next().unwrap();
        let outcome = hands.next().unwrap();

        let player1 = match player1 {
            "A" => Hand::Rock,
            "B" => Hand::Paper,
            "C" => Hand::Scissors,
            _ => panic!("Invalid hand"),
        };

        let outcome = match outcome {
            "Z" => Outcome::Win,
            "X" => Outcome::Lose,
            "Y" => Outcome::Draw,
            _ => panic!("Invalid outcome"),
        };

        Match::new(player1, None, Some(outcome))
    }).collect()
}

#[aoc(day2, part2)]
pub fn part2(input: &[Match]) -> u32 {
    input.iter().map(|m| m.fix()).sum()
}