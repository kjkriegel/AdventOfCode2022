use std::collections::VecDeque;

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum OpCode {
    Mul,
    Add
}
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Test {
    divisible_by: usize,
    if_true: usize,
    if_false: usize
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Operation {
    op: OpCode,
    operand: isize
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Monkey {
    items: VecDeque<usize>,
    operation: Operation,
    test: Test,
    inspected_items: usize,
}

#[aoc_generator(day11)]
pub fn input_generator(input: &str) -> Vec<Monkey> {
    input.split("\n\n").map(|block| {
        let mut lines = block.lines().skip(1);
        let items = lines.next().unwrap().trim().strip_prefix("Starting items: ").unwrap().split(", ").map(|item| item.parse().unwrap()).collect::<VecDeque<usize>>();
        let mut opline = lines.next().unwrap().trim().strip_prefix("Operation: new = old ").unwrap().split(' ');
        let operation = match opline.next().unwrap() {
            "+" => Operation { op: OpCode::Add, operand: opline.next().unwrap().parse().unwrap_or(-1) },
            "*" => Operation { op: OpCode::Mul, operand: opline.next().unwrap().parse().unwrap_or(-1) },
            _ => panic!("Unknown operation")
        };
        let test_div: usize = lines.next().unwrap().trim().strip_prefix("Test: divisible by ").unwrap().parse().unwrap();
        let test_true: usize = lines.next().unwrap().trim().strip_prefix("If true: throw to monkey ").unwrap().parse().unwrap();
        let test_false: usize = lines.next().unwrap().trim().strip_prefix("If false: throw to monkey ").unwrap().parse().unwrap();
        let test = Test { divisible_by: test_div, if_true: test_true, if_false: test_false };
        Monkey { items, operation, test, inspected_items: 0 }
    }).collect()
}

#[aoc(day11, part1)]
pub fn part1(input: &[Monkey]) -> usize {
    const ROUNDS: usize = 20;
    let mut monkeys = input.to_vec();
    let mut transfers: Vec<(usize, usize)> = Vec::with_capacity(32);
    for _ in 0..ROUNDS {
        for monkey_id in 0..monkeys.len() {
            transfers.clear();
            let monkey = monkeys.get_mut(monkey_id).unwrap();
            for item in &mut monkey.items.drain(..) {
                monkey.inspected_items += 1;
                let mut new_item = match monkey.operation.op {
                    OpCode::Add => match monkey.operation.operand {
                        -1 => item + item,
                        x => item + x as usize
                    },
                    OpCode::Mul => match monkey.operation.operand {
                        -1 => item * item,
                        x => item * x as usize
                    }
                };
                new_item /= 3;
                if new_item % monkey.test.divisible_by == 0 {
                    transfers.push((monkey.test.if_true, new_item));
                } else {
                    transfers.push((monkey.test.if_false, new_item));
                }
            }
            for (target, item) in &transfers {
                monkeys[*target].items.push_back(*item);
            }
        }
    }
    let mut counts = monkeys
        .iter()
        .map(|m| m.inspected_items)
        .collect::<Vec<usize>>();
    counts.sort_unstable();
    counts.iter().rev().take(2).product::<usize>()
}

#[aoc(day11, part2)]
pub fn part2(input: &[Monkey]) -> usize {
    const ROUNDS: usize = 10000;
    let mut monkeys = input.to_vec();
    let mut transfers: Vec<(usize, usize)> = Vec::with_capacity(32);
    let modulus = monkeys.iter()
        .map(|monkey| monkey.test.divisible_by)
        .product::<usize>();
    for _ in 0..ROUNDS {
        for monkey_id in 0..monkeys.len() {
            transfers.clear();
            let monkey = monkeys.get_mut(monkey_id).unwrap();
            for item in &mut monkey.items.drain(..) {
                monkey.inspected_items += 1;
                let mut new_item = match monkey.operation.op {
                    OpCode::Add => match monkey.operation.operand {
                        -1 => (item + item) % modulus,
                        x => (item + x as usize) % modulus
                    },
                    OpCode::Mul => match monkey.operation.operand {
                        -1 => (item * item) % modulus,
                        x => (item * x as usize) % modulus
                    }
                };
                // new_item /= 3;
                if new_item % monkey.test.divisible_by == 0 {
                    transfers.push((monkey.test.if_true, new_item));
                } else {
                    transfers.push((monkey.test.if_false, new_item));
                }
            }
            for (target, item) in &transfers {
                monkeys[*target].items.push_back(*item);
            }
        }
    }
    let mut counts = monkeys
        .iter()
        .map(|m| m.inspected_items)
        .collect::<Vec<usize>>();
    counts.sort_unstable();
    counts.iter().rev().take(2).product::<usize>()
}
