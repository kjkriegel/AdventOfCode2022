use itertools::Itertools;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct AssignmentPair {
    pub left: std::ops::Range<u32>,
    pub right: std::ops::Range<u32>,
}

#[aoc_generator(day4)]
pub fn input_generator(input: &str) -> Vec<AssignmentPair> {
    input.split(&['\n', '-', ','])
        .tuples()
        .map(|(left_start, left_end, right_start, right_end)| {
            AssignmentPair {
                left: left_start.parse::<u32>().unwrap()..left_end.parse::<u32>().unwrap(),
                right: right_start.parse::<u32>().unwrap()..right_end.parse::<u32>().unwrap(),
            }
        }).collect::<Vec<AssignmentPair>>()

}

#[aoc(day4, part1)]
pub fn part1(input: &[AssignmentPair]) -> u32 {
    input.iter().map(|pair| {
        u32::from(pair.left.start >= pair.right.start && pair.left.end <= pair.right.end || pair.right.start >= pair.left.start && pair.right.end <= pair.left.end)
    }).sum()
}

#[aoc(day4, part2)]
pub fn part2(input: &[AssignmentPair]) -> u32 {
    input.iter().map(|pair| {
        u32::from(!(pair.left.start > pair.right.end || pair.right.start > pair.left.end))
    }).sum()
}