use itertools::Itertools;
use regex::Regex;

pub type Crate = char;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Command {
    pub amount: usize,
    pub origin: usize,
    pub destination: usize,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Ship {
    pub items: [Vec<char>; 9],
    pub commands: Vec<Command>,
}

#[aoc_generator(day5)]
pub fn input_generator(input: &str) -> Ship {
    let (cargo, commands): (&str, &str) = input.split("\n\n").collect_tuple().unwrap();

    let mut items: [Vec<char>; 9] = Default::default();
    for line in cargo.lines().rev().skip(1) {
        for (i, c) in line.chars().skip(1).step_by(4).enumerate() {
            if c != ' ' {
                items[i].push(c);
            }
        }
    }
    
    let re = Regex::new(r"^move (\d+) from (\d+) to (\d+)$").unwrap();
    let commands = commands.lines().map(|line| {
        let caps = re.captures(line).unwrap();
        Command {
            amount: caps[1].parse::<usize>().unwrap(),
            origin: caps[2].parse::<usize>().unwrap(),
            destination: caps[3].parse::<usize>().unwrap(),
        }
    }).collect::<Vec<Command>>();

    Ship {
        items,
        commands,
    }
}

#[aoc(day5, part1)]
pub fn part1(input: &Ship) -> String {
    let mut input = input.clone();
    for command in &input.commands {
        let current_len = input.items[command.origin - 1].len();
        let move_items = input.items[command.origin - 1]
            .drain(current_len-command.amount..current_len).rev().collect::<Vec<Crate>>();
        input.items[command.destination - 1].extend(move_items);
    }
    input.items.iter().map(|v| v.last().unwrap()).collect::<String>()
}

#[aoc(day5, part2)]
pub fn part2(input: &Ship) -> String {
    let mut input = input.clone();
    for command in &input.commands {
        let current_len = input.items[command.origin - 1].len();
        let move_items = input.items[command.origin - 1]
            .drain(current_len-command.amount..current_len).collect::<Vec<Crate>>();
        input.items[command.destination - 1].extend(move_items);
    }
    input.items.iter().map(|v| v.last().unwrap()).collect::<String>()
}
