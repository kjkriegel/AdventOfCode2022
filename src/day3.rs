use std::collections::HashSet;
use itertools::Itertools;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
#[repr(u8)]
pub enum Item {
    a = 1,
    b,
    c,
    d,
    e,
    f,
    g,
    h,
    i,
    j,
    k,
    l,
    m,
    n,
    o,
    p,
    q,
    r,
    s,
    t,
    u,
    v,
    w,
    x,
    y,
    z,
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct Rucksack {
    pub compartment1: Vec<Item>,
    pub compartment2: Vec<Item>,
}

#[aoc_generator(day3)]
pub fn part1_generator(input: &str) -> Vec<Rucksack> {
    input.split_whitespace().map(|line| {
    let compartments = line.chars()
            .map(|c| match c {
                'a' => Item::a,
                'b' => Item::b,
                'c' => Item::c,
                'd' => Item::d,
                'e' => Item::e,
                'f' => Item::f,
                'g' => Item::g,
                'h' => Item::h,
                'i' => Item::i,
                'j' => Item::j,
                'k' => Item::k,
                'l' => Item::l,
                'm' => Item::m,
                'n' => Item::n,
                'o' => Item::o,
                'p' => Item::p,
                'q' => Item::q,
                'r' => Item::r,
                's' => Item::s,
                't' => Item::t,
                'u' => Item::u,
                'v' => Item::v,
                'w' => Item::w,
                'x' => Item::x,
                'y' => Item::y,
                'z' => Item::z,
                'A' => Item::A,
                'B' => Item::B,
                'C' => Item::C,
                'D' => Item::D,
                'E' => Item::E,
                'F' => Item::F,
                'G' => Item::G,
                'H' => Item::H,
                'I' => Item::I,
                'J' => Item::J,
                'K' => Item::K,
                'L' => Item::L,
                'M' => Item::M,
                'N' => Item::N,
                'O' => Item::O,
                'P' => Item::P,
                'Q' => Item::Q,
                'R' => Item::R,
                'S' => Item::S,
                'T' => Item::T,
                'U' => Item::U,
                'V' => Item::V,
                'W' => Item::W,
                'X' => Item::X,
                'Y' => Item::Y,
                'Z' => Item::Z,
                _ => unreachable!()
            }).collect::<Vec<Item>>();
            let mut compartments = compartments.chunks_exact(line.len() / 2);
            let ruck = Rucksack {
                compartment1: compartments.next().unwrap().to_vec(),
                compartment2: compartments.next().unwrap().to_vec(),
            };
            assert_eq!(compartments.remainder().len(), 0);
            ruck
    }).collect::<Vec<Rucksack>>()
}


#[aoc(day3, part1)]
pub fn part1(input: &[Rucksack]) -> u32 {
    input.iter().map(|ruck| {
        let comp1: HashSet<Item> = HashSet::from_iter(ruck.compartment1.iter().copied());
        let comp2: HashSet<Item> = HashSet::from_iter(ruck.compartment2.iter().copied());
        comp1.intersection(&comp2).next().map(|item| *item as u32).unwrap_or(0)
    }).sum()
}

#[aoc(day3, part2)]
pub fn part2(input: &[Rucksack]) -> u32 {
    input.iter().map(|ruck| {
        ruck.compartment1.iter().copied().chain(ruck.compartment2.iter().copied()).collect::<Vec<Item>>()
    }).tuples().map(|(ruck1, ruck2, ruck3)| {
        let comp1: HashSet<Item> = HashSet::from_iter(ruck1.iter().copied());
        let comp2: HashSet<Item> = HashSet::from_iter(ruck2.iter().copied());
        let comp3: HashSet<Item> = HashSet::from_iter(ruck3.iter().copied());
        let comp1_2: HashSet<Item> = comp1.intersection(&comp2).copied().collect();
        comp1_2.intersection(&comp3).next().map(|item| *item as u32).unwrap_or(0)
    }).sum()
}
