#[aoc(day1, part1)]
pub fn part1(input: &str) -> i32 {
    input
        .split("\n\n")
        .map(|elf| {
            elf.split('\n')
                .map(|line| line.parse::<i32>().unwrap())
                .sum()
        })
        .max()
        .unwrap()
}

#[aoc(day1, part2)]
pub fn part2(input: &str) -> u32 {
    let mut sorted_input = input
        .split("\n\n")
        .map(|elf| {
            elf.split('\n')
                .map(|line| line.parse::<u32>().unwrap())
                .sum()
        })
        .collect::<Vec<u32>>();

    sorted_input.sort_unstable();
    sorted_input.reverse();

    println!("{:#?}", sorted_input);

    sorted_input[..3].iter().sum()
}
